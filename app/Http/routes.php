<?php
Route::group(['prefix'=>'api'], function(){
    Route::get('get_all_blog', 'BlogController@all');
    Route::post('create', 'BlogController@create');
    Route::post('edit/{blog_id}', 'BlogController@edit');
    Route::post('delete', 'BlogController@delete');
});


Route::get('/', 'BlogController@index');

Route::get('register', 'HomeController@getRegister');
Route::post('register', 'HomeController@postRegister');

Route::post('comment', 'BlogController@postComment');

Route::get('{slug}', 'BlogController@handler')->where('slug', '(?!admin).+');
