<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use App\User;
use Cartalyst\Sentinel\Native\Facades\Sentinel;

class HomeController extends Controller {

    public function getRegister() {
        return \View::make('register');
    }

    public function postRegister(Input $input) {
        $input = $input->get();
        $error = [];

        $email = $input['email'];
        if(User::where('email', $email)->first()) {
            $error['email'] = 'already taken';
        }

        if($input['password'] != $input['password2']) {
            $error['password'] = 'mismatch';
        }

        if($error) {
            return \View::make('register', compact('email', 'error'));
        }

        $user = Sentinel::register([
            'email'=>$email,
            'password'=>$input['password'],
            'first_name'=>$input['first_name'],
            'last_name'=>$input['last_name'],
        ], true);

        Sentinel::findRoleByName('Blogger')->users()->attach($user);

        session()->flash('registered_user', $user);

        return redirect('/admin');
    }
}
