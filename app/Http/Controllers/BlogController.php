<?php
namespace App\Http\Controllers;

use App\Blog;
use App\Comment;
use Illuminate\Support\Facades\Input;
use Request;

class BlogController extends Controller {
    const
        STATUS_SUCCESS               = '200:success', // success
        STATUS_BAD_REQUEST           = '400:bad_request', // validation error
        STATUS_FORBIDDEN             = '403:forbidden', // invalid api token
        STATUS_REQUEST_TIME_OUT      = '408:request_time_out', // request takes time more than 10 seconds
        STATUS_INTERNAL_SERVER_ERROR = '500:internal_server_error'; //  internal server error

    public function index() {
        return \View::make('index', ['blogs'=>Blog::get()]);
    }

    public function handler($slug = null) {
        if($blog = Blog::where('slug', $slug)->first()) {
            $blog->publisher;
            $blog->comments;
            $blog->publishedBy = trim("{$blog->publisher->first_name} {$blog->publisher->last_name}");
            return \View::make('blog-default', $blog->toArray());
        }

        return \Response::view('errors.404', [], 404);
    }

    public function all() {
        return Blog::get()->toArray();
    }

    public function create(Input $input) {
        $data = $input->get('data');
        if(!$data) {
            self::respond(self::STATUS_BAD_REQUEST);
        }

        try {
            Blog::unguard();
            $blog = Blog::create($input->get('data'));
        } catch(\Exception $e) {
            self::respond(self::STATUS_BAD_REQUEST);
        }

        return $blog;
    }

    public function edit(Input $input, $blog_id) {
        if(
            !($data = $input->get('data')) OR
            !($blog = Blog::find($blog_id))
        ) {
            self::respond(self::STATUS_BAD_REQUEST);
        }

        try {
            unset($data['id']);
            $blog->update($data);
        } catch(\Exception $e) {
            self::respond(self::STATUS_BAD_REQUEST);
        }

        return $blog;
    }

    public function delete(Input $input) {
        if(
            !($data = $input->get('data'))
        ) {
            self::respond(self::STATUS_BAD_REQUEST);
        }

        try {
            Blog::where($data)->delete();
        } catch(\Exception $e) {
            self::respond(self::STATUS_BAD_REQUEST);
        }

        self::respond(self::STATUS_SUCCESS);
    }

    public static function respond($status) {
        list($code, $mssg) = explode(':', $status);
        abort($code, $mssg);
    }

    public function postComment(Input $input) {
        Comment::unguard();
        Comment::create([
            'blog_id' => $input->get('blog_id'),
            'content' => $input->get('content'),
        ]);
        if($referer = Request::server('HTTP_REFERER')) {
            return redirect($referer);
        }
    }
}
