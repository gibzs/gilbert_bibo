<?php
use \Cartalyst\Sentinel\Laravel\Facades\Sentinel;

\Admin::model('App\Blog')->title('Blogs')->alias('blogs')->display(function ()
{
    $display = AdminDisplay::table();

    $display->apply(function($query) {
        if(!Sentinel::getUser()->hasAccess('superadmin'))
            $query->where('user_id', Sentinel::getUser()->id);
    });

    $display->columns([
        #Column::checkbox(),
        Column::string('id')->label('ID'),
        Column::string('title')->label('Title'),
        Column::custom()->label('Slug')->callback(function($row){
            return '<a target="_blank" href="'.URL::to('/'.$row->slug).'">'.$row->slug.'</a>';
        })
    ]);

    $display->parameters([
       'user_id'=>\Sentinel::getUser()->id,
    ]);
    return $display;
})->createAndEdit(function ()
{
    $form = AdminForm::form();
    $form->items([
        #'Details' => [
            FormItem::text('title', 'Title')->required(),
            FormItem::text('slug', 'Slug')->required()->unique(),
            FormItem::text('heading', 'Heading')->required(),
            FormItem::text('subheading', 'Subheading'),
            FormItem::ckeditor('content', 'Content'),
            FormItem::custom()->display(function ($row)
            {
                return '<input type="hidden" name="user_id" value="'.($row->user_id ?: \Sentinel::getUser()->id).'">';
            })->callback(function ($row)
            {
                $row->user_id = ($row->user_id ?: \Sentinel::getUser()->id);
            })
        #],
        #'Permissions' => [
            //FormItem::permissions('permissions', 'Permissions')
        #],
    ]);
    return $form;
});