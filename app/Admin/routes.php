<?php
if($loginUser = session()->pull('registered_user')) {
    \Sentinel::login($loginUser);
}

if(isset($_SESSION)) {
    if ($loggedInUser = \Sentinel::getUser()) {
        $_SESSION['loggedInUser'] = $loggedInUser->getAttributes();
    } else {
        unset($_SESSION['loggedInUser']);
    }
}

Route::get('', [
    'as' => 'admin.dashboard',
    function ()
    {
        $content = '';
        return Admin::view($content, 'Dashboard');
    }
]);

session(['registered_user'=>null]);