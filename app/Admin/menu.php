<?php
use \Cartalyst\Sentinel\Laravel\Facades\Sentinel;

if(($user = Sentinel::getUser()) && $user->hasAccess('superadmin')) {
    Admin::menu()->label('User Management')->icon('fa-book')->items(function () {
        Admin::menu('SleepingOwl\Admin\Model\User')->icon('fa-user');
        #Admin::menu('Cartalyst\Sentinel\Roles\EloquentRole')->icon('fa-users');
        #Admin::menu('SleepingOwl\Admin\Model\Permission')->icon('fa-users');
    });
}

Admin::menu()->url('blogs')->label('Blogs')->icon('fa-pencil');
