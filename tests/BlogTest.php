<?php

class BlogTest extends TestCase
{
    public function test()
    {
        # retrieve all blogs
        echo "\n/api/get_all_blog:\n";
        $response = $this->call('GET', '/api/get_all_blog');
        $this->assertEquals(200, $response->status());
        echo $response->getContent()."\n";

        # create a sample blog
        $data = ['title'=>'Page Title', 'slug'=>md5(time()), 'heading'=>'Page Heading'];
        echo "\n/api/create: using ".json_encode($data)." :\n";
        $response = $this->call('POST', '/api/create', compact('data'));
        $this->assertEquals(200, $response->status());
        $blog = json_decode($response->getContent());
        print_r($blog);

        # edit the created blog
        $data = ['title'=>'Page Title', 'slug'=>'edited_'.$blog->slug, 'heading'=>'Page Heading'];
        echo "\n/api/edit/{$blog->id}: using ".json_encode($data)." :\n";
        $response = $this->call('POST', '/api/edit/'.$blog->id, compact('data'));
        $this->assertEquals(200, $response->status());
        $blog = json_decode($response->getContent());
        print_r($blog);

        # delete the created blog
        $data = ['id' => $blog->id];
        echo "\n/api/delete: using ".json_encode($data)." :\n";
        $response = $this->call('POST', '/api/delete', compact('data'));
        $this->assertEquals(200, $response->status());
        echo "ok\n";
    }
}
