# OVERVIEW
This application uses Laravel 5.2 framework, with *pseudoagentur/soa-sentinel* library for admin panel

## Requirements:
 - PHP >= 5.5.9
 - OpenSSL (PHP Extension)
 - PDO (PHP Extension)
 - Mbstring (PHP Extension)
 - Tokenizer (PHP Extension)
 - Composer ([install](https://getcomposer.org/download/))
 - Laravel 5.2 ([install](https://laravel.com/docs/5.2))


# SETUP
 1. Clone repository to desired directory and navigate into it

		git clone https://gibzs@bitbucket.org/gibzs/gilbert_bibo.git
		cd  /path/to/gilbert_bibo

 2. Make sure the web server can write into the `storage` & `bootstrap/cache` directories, and all of its files and subdirectories:

		sudo chgrp -R $WEBSERVER storage bootstrap/cache
		sudo chmod -R ug+rwx storage bootstrap/cache
	*where $WEBSERVER is the username of your webserver (apache / _www / ...)*


 3. `composer update` (this may take a while)

 4. Create a database.

 5. Rename the file `.env.example` into `.env`, and modify the following configurations:

		DB_CONNECTION=mysql
		DB_HOST=127.0.0.1
		DB_PORT=3306
		DB_DATABASE=your_created_database
		DB_USERNAME=your_db_user_name
		DB_PASSWORD=your_db_user_password

 6. Install the admin panel:

        php artisan admin:install

 7. Serve the `public` directory.


# ADMIN PANEL
On your browser, navigate to `//yourdomain.local/admin` and login using `admin@soa.backend` / `password`.

# UNIT TESTING
On terminal, go to cloned project directory, and execute:

		vendor/bin/phpunit
