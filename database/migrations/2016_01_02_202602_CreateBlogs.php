<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function($table)
        {
            $table->increments('id');
            $table->string('title');
            $table->string('slug')->unique();
            $table->binary('content');
            $table->integer('user_id')->unsigned();
            $table->string('heading');
            $table->string('subheading');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
