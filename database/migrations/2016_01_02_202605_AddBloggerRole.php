<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddBloggerRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('roles')->insert([
            'slug'=>'blogger',
            'name'=>'Blogger',
            'permissions'=>'{"admin.dashboard":true,"controlpanel":true,"admin.blogs.view":true,"admin.blogs.create":true,"admin.blogs.edit":true,"admin.blogs.destroy":true}',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
